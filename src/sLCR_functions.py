'''
__name__ = 'carlos.company.bioinf[at]gmail.com'
__date__ = '20220203'
__version__ = 'beta 0.1'
'''

# Import data
import os, pysam,math
import pandas as pd
import numpy as np
import multiprocessing as mp
from pybedtools import BedTool
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.SeqUtils import GC
from Bio.Seq import Seq
from numba import jit

def correlate_closest_cis(path_genes, path_cis_file, name_file, path_results ):

    '''
    Function to divide the genome based on cis regulatory regions (e.g. CTCF )

    - Input:
        * Path to a 6 fields BED ( only ) -> chr/st/end/gene_name/score/strand
        * Path to a regulatory cis file -> chr/st/end/cis_id (e.g. a description - not important)

    - Output:
        * Dataframe gene-bed file extended to the closest cis regulatory region -> chr/st/end/gene
    '''

    # Read bed files & sort
    genes = BedTool(path_genes)
    genes = genes.sort()
    cis = BedTool(path_cis_file)
    cis = cis.sort()

    # Define Cis regulatory regions up-/downstream
    up_cis_genes = genes.closest(cis, id=True, io=True, D="a").to_dataframe()
    down_cis_genes = genes.closest(cis, iu=True, io=True, D="a").to_dataframe()
    cis_genes = up_cis_genes.append(down_cis_genes)

    # Closest cis-regulatory region upstream/downstream for each gene
    new_genes = genes.to_dataframe()
    for i in cis_genes.index.unique():
        df_gene = cis_genes.loc[i, :]

        # If there is not overlap between Cis-regulatory regions then removes the entry (gene or region)
        if not df_gene.blockCount.unique()[0] == '.':
            if df_gene.strand.unique()[0] == '-':
                # Transform the initial genomic location based on the defined cis region
                minus_start = df_gene.start.unique()[0] - df_gene.blockSizes.unique()[1]
                add_end = df_gene.end.unique()[0] - df_gene.blockSizes.unique()[0]

                # Modify the original gene table with the regions
                new_genes.loc[new_genes['name'] == df_gene.name.unique()[0], 'start'] = minus_start
                new_genes.loc[new_genes['name'] == df_gene.name.unique()[0], 'end'] = add_end
            
            if df_gene.strand.unique()[0] == '+':
                # Transform the initial genomic location based on the defined cis region
                minus_start = df_gene.start.unique()[0] + df_gene.blockSizes.unique()[0]
                add_end = df_gene.end.unique()[0] + df_gene.blockSizes.unique()[1]

                # Modify the original gene table with the regions
                # To change here the data, so it will take the minus
                new_genes.loc[new_genes['name'] == df_gene.name.unique()[0], 'start'] = minus_start
                new_genes.loc[new_genes['name'] == df_gene.name.unique()[0], 'end'] = add_end
        else: print "Removed %s" % (df_gene.name.unique()[0])

    # Message after complete the analysis
    print '[ cis-gene-cis Finished ] Complete Region determination'

    # Save Data
    path_file  = path_results + '/' + name_file  + '_coreg_cis.csv'
    new_genes.to_csv(path_file, sep='\t', index=False, header=False)

    # Return file
    return new_genes


def sliding_window(input_genes, name_file, path_results, win_width=150, win_span=50, genome_filter = False):

    '''
    Function to divide regions in small regions define by sliding window

    - Input:
        * correlate_closest_cis output file
    - Output:
        * Cis-window-extended dataframe -> chr/st/end/gene
    '''

    # Divide gene region
    def slide_fun(region):
        region = input_genes.iloc[region, :]
        # Define the regions
        s = region['start']
        e = region['start'] + win_width

        # Divide the regions
        new_i = []
        while s < region['end']:
            # Define the new set
            s += win_span
            e += win_span
            row = pd.DataFrame(
                {
                    'chrom': region['chrom'],
                    'start': s,
                    'end': e,
                    'name': region['name'],
                    'score': region['score'],
                    'strand': region['strand']
                }, index=[0])
            cols = ['chrom', 'start', 'end', 'name', 'score', 'strand']
            row = row.loc[:, cols]
            new_i.append(row)
        # Combine all the regions
        gene_slide_concat = pd.concat(new_i)
        return gene_slide_concat

    # Divide each region based on the gene
    slide_window = map(slide_fun, input_genes.index)
    gene_slide_w = pd.concat(slide_window)

    # Remove overlaping regions
    dup_row = gene_slide_w.duplicated(subset=['chrom', 'start', 'end'], keep='first')
    gene_slide_w = gene_slide_w.assign(to_remove=dup_row)
    gene_slide_w = gene_slide_w[gene_slide_w.to_remove == False]
    gene_slide_w = gene_slide_w.drop('to_remove', axis=1)

    # Return
    print '[ Sliding Window Finished ] Complete Region Division'


    # Filter regions using a custom_genome (e.g. ATAC-seq location )
    if(genome_filter):
        # BedTools intersect of the region
        gene_slide_w = BedTool.from_dataframe(gene_slide_w)
        custom_genome = BedTool(genome_filter).sort()

        # Substitute previous with the filter by external region. Complete CRE will stay (e.g. 150 pb)
        gene_slide_w = gene_slide_w.intersect(custom_genome, wa=True,f=0.5)
        gene_slide_w = gene_slide_w.sort().to_dataframe()

        # Remove overlaping regions
        dup_row = gene_slide_w.duplicated(subset=['chrom', 'start', 'end'], keep='first')
        gene_slide_w = gene_slide_w.assign(to_remove=dup_row)
        gene_slide_w = gene_slide_w[gene_slide_w.to_remove == False]
        gene_slide_w = gene_slide_w.drop('to_remove', axis=1)

    # Save Data
    path_file  = path_results + '/' + name_file  + '_coreg_cis.wind.csv'
    gene_slide_w.to_csv(path_file, sep='\t', index=False, header=False)

    return gene_slide_w

def modify_refGene_annot(path_file, is_tss=True):
    '''
    Read a bed file and converts to DataFrame, following a Bed format
    > chr:st:end:annot:score:strand

    - Input: pathFile
    - Output: Bed format
    '''

    # Read data from refGene
    refGene = pd.read_csv(path_file, sep='\t', header=None).iloc[:, [2, 4, 5, 12, 3]]
    refGene.columns = ['chrom', 'start', 'end', 'name', 'strand']
    refGene['score'] = np.repeat(0, refGene.shape[0])
    refGene = refGene[['chrom', 'start', 'end', 'name', 'score', 'strand']]

    # Asssign the TSS based on strand
    if is_tss:
        refGene['end'] = map(
            lambda x: (int(refGene.loc[x, 'start']) + 1) if refGene.loc[x, "strand"] == "+" else refGene.loc[x, 'end'],
            list(refGene.index))
        refGene['start'] = map(
            lambda x: (int(refGene.loc[x, 'end']) - 1) if refGene.loc[x, "strand"] == "-" else refGene.loc[x, 'start'],
            list(refGene.index))
    else:
        # Assign TES based on strand
        refGene['start'] = map(
            lambda x: (int(refGene.loc[x, 'end']) - 1) if refGene.loc[x, "strand"] == "+" else refGene.loc[x, 'start'],
            list(refGene.index))
        refGene['end'] = map(
            lambda x: (int(refGene.loc[x, 'start']) + 1) if refGene.loc[x, "strand"] == "-" else refGene.loc[x, 'end'],
            list(refGene.index))

    return refGene

def correlate_closest_TSS(path_refGene, sld_region,cpu,name_file, path_results):
    '''
    Function to evaluate differences to TSS/TES regions

    - Input: Sliding window (Pandas DataFrame)
    - Output: Bed format

    The original code associate the TSS to (+) txStart-txStart and TES to (+) txEnd-txEnd (and opposite for - ).
    Then overlaps to the closest TSS using a window of 100Mbp. Then annotates the closes to the TSS

    The same effect I can use closest
    '''

    # Read/convert DataFrame to bedFile - convert_bed()
    bed_sld_region = BedTool.from_dataframe(sld_region).sort().to_dataframe()
    bed_refGene = BedTool.from_dataframe(modify_refGene_annot(path_refGene)).sort().to_dataframe()
    bed_refGene_tes = BedTool.from_dataframe(modify_refGene_annot(path_refGene, is_tss=False)).sort().to_dataframe()
    bed_sld_region.loc[:, 'score'] = map(
        lambda x: bed_sld_region.iloc[x, 1] + (bed_sld_region.iloc[x, 1] - bed_sld_region.iloc[x, 2]),
        list(bed_sld_region.index))

    # Asign Genes
    # Function: reads chrom, start, end, name
    @jit
    def find_closest(index_region):
        # Define series
        region = bed_sld_region.loc[index_region, :]

        # Find the closest to the TSS
        chr_reg = bed_refGene[bed_refGene['chrom'] == region['chrom']].sort_values(by='start').reset_index()
        closest_tss = abs((chr_reg.start - region['score'])).sort_values()
        closest_gene = chr_reg.iloc[closest_tss.index[0], 4]

        # TODO: add here if they overlap on the TES . Does overlap in that region.
        chr_reg = bed_refGene_tes[bed_refGene['chrom'] == region['chrom']].sort_values(by='start').reset_index()
        closest_tes = abs((chr_reg.start - region['score'])).sort_values()
        closest_tes_gene = chr_reg.iloc[closest_tes.index[0], 4]

        # Select the closest
        region['TSS_closest_gene'] = closest_gene
        region['TSS_dist'] = closest_tss[closest_tss.index[0]]
        region['TES_closest_gene'] = closest_tes_gene
        region['TES_dist'] = closest_tes[closest_tes.index[0]]
        # region = pd.DataFrame([region])
        return region

    # Generate the list
    pool = mp.Pool(processes=int(cpu))
    assign_genes = pool.map(find_closest, bed_sld_region.index)
    assign_genes = pd.concat(map(lambda x: pd.DataFrame([x]), assign_genes))

    # # Closest BedTools
    # assign_genes = bed_sld_region.closest(bed_refGene, s=False,D='a').to_dataframe()
    #
    # # Match Genes with the closest one
    # # Function to substitute the entry in those which the gene does not correspond with the data.
    # # Combine this data in order to generate a data frame. Then select those that are important for you.
    # assign_genes[3] = map(lambda x: assign_genes.loc[x,9] if assign_genes.loc[x,3] !=  assign_genes.loc[x,9] else assign_genes.loc[x,3], list(assign_genes.index))

    # Save the data (Strand is by Closest TSS )
    # coreg_region = assign_genes.loc[:,[0,1,2,3,12,11]]
    # coreg_region = coreg_region.rename( columns = { 0:'chrom',1:'start',2:'end',3:'name',12:'score',11:'strand' }  )

    print '[ Annotation Finished ] Complete region annotation to the closest TSS '

    # Save Data
    path_file  = path_results + '/' + name_file  + '_coreg_annotated.cis.wind.csv'
    assign_genes.to_csv(path_file, sep='\t', index=False, header=False)

    return assign_genes

def extract_sequence(annot_df, ref_seq, name_file, path_results ):
    '''
    Function to extract the sequences once the region (150 sliding window ) has been annotated to the closest TSS/TES

    - Input: window region data, with the annotation to nearest TSS (Pandas Data Frame)
    - Output: fasta with sequences (Input to MEME toolSuit )

    * It does allow some overlap

        TO DO :
        - Extract the sequences here
    '''

    # Filter annot_df based on a Custom genome
    # if(genome_filter):
        # Input is the genome

    # Read reference sequence
    ref_fa = pysam.Fastafile(ref_seq)
    annot_df = annot_df.reset_index()

    # Obtain the each data sequence
    to_write = []
    for i in annot_df.index:
        # header fasta
        fa_header = annot_df.loc[i, 'chrom'] + ':' + str(annot_df.loc[i, 'start']) + '-' + str(annot_df.loc[i, 'end'])
        seq1 = ref_fa.fetch(annot_df.loc[i, 'chrom'], int(annot_df.loc[i, 'start']) - 1, int(annot_df.loc[i, 'end']))

        # Build Sequence using
        sr = SeqRecord(Seq(seq1), fa_header, '', '')
        to_write.append(sr)

    # Save the sequence
    path_file  = path_results + '/' + name_file  + '.fa'
    SeqIO.write(to_write, path_file, "fasta")

    print '[ Sequence Finished ] Obtain sequences region'

def call_FIMO_local(meme_path, tf_list, path_results, name_file):
    '''
    Function to call FIMO (MEME tool suit).

    * Some of the input files should be in a folder call Data (or Give the option to be save there)

    for i  in `ls meme/JASPAR2018_CORE_vertebrates_non-redundant_pfms_meme/*.meme`
    do
        n=`echo $i | cut -f3 -d '/' | sed s/'.meme'/'_fimo'/`
        fimo  --oc results/$n --max-stored-scores 10000000 --no-qvalue $i coreg_regions.hg19.windows.fa
    done

    for m in $(cat motifs_names.txt);
    do
        fimo --output-pthresh 1e-4 --no-qvalue --max-stored-scores 10000000 --motif "$m" --text "$motifs_file" coreg_regions.hg19.windows.fa > results/"$m"
    done
    '''

    # Create a folder to save TFBS data
    dir_name = path_results + '/results'
    if not os.path.exists(dir_name):  # if the directory does not exist
        os.makedirs(dir_name)
    else:
        cmd_rm = "rm -r %s" % (dir_name)
        os.system(cmd_rm)
        os.makedirs(dir_name)

    # FIMO command line
    name_fasta  = path_results + '/' + name_file  + '.fa'
    cmd = "while read m; do fimo --o %s/$m --no-qvalue --max-stored-scores 10000000 --motif $m --text %s %s > %s/$m ; done < %s &> tfbs_log.txt" % (dir_name, meme_path, name_fasta, dir_name, tf_list)
    os.system(cmd)
    print '[ FIMO Finished ] Complete TFBS analysis'

    # Merge all the results from fimo in one
    cmd = "cat %s/* | grep -v Motif > %s/%s_map" % (dir_name, path_results, name_file)
    os.system(cmd)

def fimo2tab(meme_path, name_file, path_results ):
    '''
    Function to call Fimo to Tab
    * Iros Function
    * Converst the FIMO output in a matrix of -log10 P.value of region/TFBS

    - Input
    HC_AHR_si	chr2:143371108-143371258	113	121	+	12.5641	2.25427e-05	GGTGCGTGC
    HC_AHR_si	chr2:143371158-143371308	63	71	+	12.5641	2.25427e-05	GGTGCGTGC
    HC_AHR_si	chr2:143371208-143371358	13	21	+	12.5641	2.25427e-05	GGTGCGTGC
    HC_AHR_si	chr2:143409708-143409858	122	130	+	11.6698	6.25381e-05	TGTGCGTGC
    HC_AHR_si	chr2:143409758-143409908	72	80	+	11.6698	6.25381e-05	TGTGCGTGC
    HC_AHR_si	chr2:143409808-143409958	22	30	+	11.6698	6.25381e-05	TGTGCGTGC

    - out put
    Region	Fimo_Best_MA0002.1.RUNX1	Fimo_Best_MA0003.1.TFAP2A	Fimo_Best_MA0004.1.Arnt	Fimo_Best_MA0006.1.Arnt::Ahr
    chr5:169169692-169169842	0	0	0	0
    chr1:183550411-183550561	0	0	0	0
    chr5:169295292-169295442	0	0	0	0
    chr10:45962165-45962315	0	0	0	0
    chrX:37627578-37627728	0	0	0	0
    chr7:115848649-115848799	0	0	0	0
    chrX:37682028-37682178	0	0	0	0
    chr2:143786258-143786408	0	0	0	0
    chr10:33609523-33609673	0	0	0	0
    '''

    # Input arguments
    fimo_path = path_results + "/" + name_file + "_map"

    # Read the motifs contained in the file
    def cleanSTR(string):
        """ Cleans a string from \r and \n """
        string = string.replace("\r", "")
        string = string.replace("\n", "")
        return string

    def filewrite(outputfile, text):
        """ Writes the content of text in outputfile file """
        f = open(outputfile, 'w')
        f.write(text)
        f.close()

    motifs = []
    f1 = open(meme_path, 'r')
    for l in f1.readlines():
        temp = l.split("MOTIF")
        if len(temp) == 2:
            motifs.append(cleanSTR(temp[1]).replace(" ", ""))
    f1.close()

    # Determine the best one
    regions_best = {}
    f2 = open(fimo_path, 'r')
    for l in f2.readlines():
        temp = l.split("\t")
        # if temp[0] != "motif_id": # To modify
        if temp[0] != "motif_id":  # To modify
            pwm = cleanSTR(temp[0])
            region = cleanSTR(temp[2])
            # region = cleanSTR(temp[1])
            # p = -1 * math.log(float(cleanSTR(temp[6])), 10)
            p = -1 * math.log(float(cleanSTR(temp[7])), 10)
            if not regions_best.has_key(region):
                regions_best[region] = {}
                for m in motifs:
                    regions_best[region][m] = 0
            if p > regions_best[region][pwm]:
                regions_best[region][pwm] = p
    f2.close()

    # Generate from here a pandas Dataframe
    out_best = "Region"
    for m in motifs:
        out_best += "\tFimo_Best_" + m
    out_best += "\n"
    for r in regions_best:
        out_best += r
        for m in motifs:
            out_best += "\t" + str(regions_best[r][m])
        out_best += "\n"

    filewrite(fimo_path + ".best.tab", out_best)
    print '[ TFBS-matrix Generated ] Selected best region for TFBS'

def sLCR_selection(ref_seq, tss_annot, name_file, path_results):
    '''
    Function that calls sLCR_pipeline_200118_v1.R (or a new version compute in python)
    Uses as input the table from FIMO (map.best.tab), and then computes which are the best TSS, builing up a new sequences

    * The output for the experimental should be a fasta file that contains:

    > sLCR Name - #1 -
    [ 4 - 5 ] 150 bp region (best ones )  +  [ region that is a TSS ]

    The natural tss is considered if +/- 1kb TSS ( the data is )
    '''


    # Input input_map_best, sld_annot
    coreg_region = path_results + "/" + name_file + "_coreg_annotated.cis.wind.csv"
    best_tfbs = path_results + "/" + name_file + "_map.best.tab"
    r_script_path = os.path.dirname(os.path.realpath(__file__)) + "/" + "sLCR_selection.R"
    cmd_sLCR = "Rscript %s %s %s %s %s %s" %(r_script_path, name_file,path_results, coreg_region ,best_tfbs , tss_annot)
    os.system(cmd_sLCR)

    # TODO: Add name to the selected sLCR in the script: sLCR_selection.R

    # Generate the sequence
    top_sclr = path_results + "/" + name_file + '_top_selection.csv'
    slcr_sel = pd.read_csv(top_sclr, index_col=0)

    # Select the first 6 : they are order
    # Read reference sequence
    ref_fa = pysam.Fastafile(ref_seq)

    # Obtain the each data sequence
    to_write = []
    to_header = []
    for i in slcr_sel.index:
        # Sequence
        seq1 = ref_fa.fetch(slcr_sel.loc[i, 'chr'], int(slcr_sel.loc[i, 'start']) - 1, int(slcr_sel.loc[i, 'end']))

        # header fasta
        row_array = slcr_sel.loc[i, :].values.tolist()
        fa_header = "_".join(str(e) for e in row_array)
        fa_header = fa_header + "_" + str(round(GC(seq1), 2))
        # Build Sequence using
        sr = SeqRecord(Seq(seq1), fa_header, '', '')
        to_write.append(sr)
        to_header.append(fa_header.replace('_', "\t"))

    # Generate file with the GC content. Save by using Pandas.
    final_sclr = path_results + "/" + name_file + '_final_selection.csv'
    with open(final_sclr, 'w') as f:
        f.write(
            "chr\tstart\tend\tname\tMidPostion\tstrand\tTSS_closest_gene\tTSS_dist\tTES_closest_gene\tTES_dist\tScore\tDiversity\tGC\n")
        for item in to_header:
            f.write("%s\n" % item)

    # Save the sequence
    name_fasta = path_results + '/' + name_file + "_sLCR_selected.fa"
    SeqIO.write(to_write, name_fasta, "fasta")
    print '[ sLCR Generated ] Pipeline Complete'


''' TODO

- Implementation of a ML selection method. 
- Summary table with the best and other statistics of the sLCR selection
- Organize sLCR avoiding reading frames
- sLCR selection implemented in R  

    # # Read the data
    # tfbs = pd.read_csv("map.best.tab", sep='\t', index_col=0)
    #
    # # Filter the values
    # tfbs = tfbs[tfbs.std(axis=1) > 0] # Rows
    # tfbs = tfbs.loc[ :, tfbs.columns[tfbs.std(axis=0) > 0]]  # columns
    #
    #
    # # Combine with the TSS annotated
    # new_index =  map(lambda x: str(sld_annot.loc[x,'chrom']) + ":" + str(sld_annot.loc[x,'start']) + "-" + str(sld_annot.loc[x,'end']) , sld_annot.index )
    # sld_annot = sld_annot.set_index(pd.Series(new_index))
    # sld_annot = sld_annot.loc[sld_annot.index.intersection(tfbs.index) , :]
    #
    #
    # # Combine
    # tfbs_annot = sld_annot.join( tfbs)
    #
    # # Calculate the score
    # # ci < - apply(d_merge.filt[, -1 * c(1:5)], 1, sum)
    # # # Filter top500 to imprpve the performance
    # # top500 < - d_merge.filt[order(-ci),][1:500, -1 * c(1:5)]
    # # sd_pwm < - apply(top500, 2, sd)
    # # top500 < - top500[, sd_pwm != 0]
    # # # filter regions with sd = 0
    # # sd_reg < - apply(top500, 1, sd)
    # # top500 < - top500[sd_reg != 0,]
    #
    #
    # # Function to calculate the best of it
    #

def sort_remove_al(input_mat):

    # Internal function to calculate sort and select the best
    def aux_fun(indv_mat):

        # Initial TFBS
        id_tfbs =  indv_mat.columns

        # Filter Values
        indv_mat = indv_mat[indv_mat.sum(axis=1) > 0]  # Rows
        indv_mat = indv_mat.loc[:, indv_mat.columns[indv_mat.sum(axis=0) > 0]]  # columns

        # Compute score
        indv_mat.apply()



    num.tfbs = scale(apply(i.mat != 0, 1, sum))
    score.tfbs = scale(apply(i.mat, 1, sum))
    select.dat = data.frame('div' = num.tfbs, 'val' = score.tfbs)

    # Sort and select
    select.dat = select.dat[order(apply(select.dat, 1, sum), decreasing=T),]
    best.name = rownames(select.dat[1,])
    names.best = n.mat[i.mat[best.name,] > 0]
    i.mat = i.mat[, -1 * match(names.best, colnames(i.mat))] % > % as.data.frame()

    # Return both
    keep.ls = list(names.best, i.mat) % > % set_names(c(best.name, 'mat'))
    return (keep.ls)
    }

    # Start the selection
    slcr_start = best_fun(mat_all)  # Start data
    slcr_list = list()  # Save

    # Run over the total
    n_iter = 0
    while (ncol(slcr_start$mat) > 1  ) {
    # Selection
    n_iter = n_iter +  1
    slcr_list[[n_iter]] = best_fun(slcr_start$mat)

    # Re-start the matrix
    slcr_start = slcr_list[[n_iter]]
    }

    # Graphical Evaluation
    print(sapply(slcr_list, function(x)
    x[1] % > % names() ) % > % length())
    slcr_mat = mat_all[sapply(slcr_list, function(x)
    x[1] % > % names() ), ]
    col.pheat = brewer.pal(9, 'Reds')
    col.pheat[c(1)] = c('white')
    id.val.mat = slcr_mat[order(apply(slcr_mat, 1, sum), decreasing=F),]

    id.val.mat = id.val.mat[, c(which(apply(id.val.mat, 2, sum) == 0),
                                rev(sapply(1:nrow(id.val.mat), function(x)
    which(id.val.mat[x,] > 0)) % > % unlist % > % unique()))]
    pheatmap(id.val.mat, cluster_rows=F, color=col.pheat,
             main='sLCR', border_color='lightgrey', cluster_cols=F, scale='none')  # ,

}
'''

#!/usr/bin/env python

'''
__name__ = 'carlos.company.bioinf[at]gmail.com'
__date__ = '20190429'
__update__ = '20190502'
__version__ = 'beta 0.1'
'''

from optparse import OptionParser
from sLCR_functions import *

def menu():
    # Generate message
    help_message = 'Pipeline to generate synthetic Locus Control Regions based on TF and gene signatures'
    parser = OptionParser(description=help_message,
                          epilog="Molecular oncology - GargiuloLab - [ carlos.company.bioinf[at]gmail.com ]",
                          usage="%prog [options] -i/--input_bed",
                          version="%prog beta 1.0")

    # Add the different options
    parser.add_option("-i", "--input_bed",
                      action='store',
                      metavar="FILE",
                      #dest="input_bed",
                      help="Signature genes in BED - 6 fields - ")

    parser.add_option("-t", "--tf_input",
                      action="store",
                      dest="tf_input",
                      #default='motifs_names.txt',
                      help="Phenotype-specific transcription factor binding motifs")

    parser.add_option("-c", "--path_cis_file",
                      action="store",
                      dest="path_cis_file",
                      #default='regulatory_regions_CTCF_binding_sites.GRCh38.p3.bed',
                      help="File to the cis-regulatory regions")

    parser.add_option("-r", "--path_refGene",
                      action="store",
                      dest="path_refGene",
                      #default='hg19.fa',
                      help="Path to the sequence annotation")

    parser.add_option("-g", "--ref_seq",
                      action="store",
                      dest="ref_seq",
                      #default='hg19_refseq_genes.txt',
                      help="Path to the assembly annotation")

    parser.add_option("-a", "--tss_annot",
                      action="store",
                      dest="tss_annot",
                      #default="hg19_primary_weissmanTSS_20181129.bed",
                      help="File with TSS sites", )

    parser.add_option("-m", "--meme_path",
                      action="store",
                      dest="meme_path",
                      #default="20130208_pwms.meme",
                      help="File with TFBS motifs in MEME format", )

    parser.add_option("-p", "--cpu",
                      action="store",
                      dest="cpu",
                      default="3",
                      help="Number of cpu", )

    parser.add_option("-n", "--name_output",
                      action="store",
                      dest="output_name",
                      default="sLCR",
                      help="Name of fasta")

    parser.add_option("-f", "--filter_custom",
                      action="store",
                      dest="custom_genome",
                      help="Bed file to filter by regions")

    (options, args) = parser.parse_args()
    return options

def main():
    o = menu()

    # Create a folder to save the data
    dir = 'results_' + o.output_name
    if not os.path.exists(dir):  # if the directory does not exist
        os.makedirs(dir)
    else:
        cmd_rm = "rm -r %s" % (dir)
	os.system(cmd_rm)
        os.makedirs(dir)


    '''
    STEP1: Generation of gene signature region 
    '''
    # Generation of the region
    btw_cis = correlate_closest_cis(path_genes=o.input_bed,
                                    path_cis_file=o.path_cis_file,
                                    name_file=o.output_name,
                                    path_results=dir)

    # Generation of the slide window
    sld_wind = sliding_window(input_genes=btw_cis,
                              name_file=o.output_name,
                              path_results=dir,
                              genome_filter = o.custom_genome)

    # Annotate the closest TSS/TES to each region
    sld_TSS_annotated = correlate_closest_TSS(path_refGene=o.path_refGene,
                                              sld_region=sld_wind,
                                              cpu=o.cpu,
                                              name_file=o.output_name,
                                              path_results=dir)

    # Extract sequences from annotated slide windows
    extract_sequence(sld_TSS_annotated,
                     ref_seq=o.ref_seq,
                     name_file=o.output_name,
                     path_results=dir)

    '''
    STEP2: TFBS Enrichment analysis 
    '''

    # TFBS enrichment analysis
    call_FIMO_local(tf_list=o.tf_input,
                    meme_path=o.meme_path,
                    path_results=dir,
                    name_file=o.output_name)

    # Selection of Best region for each TFBS
    fimo2tab(meme_path=o.meme_path,
             name_file=o.output_name,
             path_results=dir)

    '''
    STEP3: Selection of the best region to conform the synthetic locus control region 
    '''
    # Generation of selected sLCR given a signature/TF
    sLCR_selection(ref_seq=o.ref_seq,
                   tss_annot=o.tss_annot,
                   path_results=dir,
                   name_file=o.output_name)

# Main script
if __name__ == '__main__':
    main()

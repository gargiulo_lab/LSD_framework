Logical design of synthetic cis-regulatory DNA [LSD]
==========

## BACKGROUND 

LSD, or logical design of synthetic cis-regulatory DNA, is a computational framework that uses phenotypic biomarkers and trans-regulatory networks as input to design reporters that mark the activity of specific cellular states and pathways. LSD, in particular, employs bulk or single-cell signature genes to define phenotype-specific locus regions delimited by user-defined boundary regions (e.g., CTCF). Then, it assesses the transcriptional binding sites on that locus in order to identify the best cis-regulatory elements that define the combinatorial custom locus.

In conclusion, LSD aims to capture core principles of cis-regulation and is broadly applicable to studying complex cell states and transcriptional regulation mechanisms.

## INPUT

### Parameters

```python

Usage: sLCR_generation.py [options] -i/--input_bed

Pipeline to generate synthetic Locus Control Regions (sLCR)

Options:
  --version             show program version number and exit
  -h, --help            show this help message and exit
  -i FILE, --input_bed=FILE
                        Signature genes in BED - 6 fields -
  -t TF_INPUT, --tf_input=TF_INPUT
                        TFBS motif names of an specific phenotype
  -c PATH_CIS_FILE, --path_cis_file=PATH_CIS_FILE
                        File to the cis-regulatory regions
  -r PATH_REFGENE, --path_refGene=PATH_REFGENE
                        Path to the sequence annotation
  -g REF_SEQ, --ref_seq=REF_SEQ
                        Path to the assembly annotation
  -a TSS_ANNOT, --tss_annot=TSS_ANNOT
                        File with TSS sites
  -m MEME_PATH, --meme_path=MEME_PATH
                        File with TFBS motifs in MEME format
  -p CPU, --cpu=CPU     Number of cpu
  -n OUTPUT_NAME, --name_output=OUTPUT_NAME
                        Name of fasta
  -f CUSTOM_GENOME, --filter_custom=CUSTOM_GENOME
                    Bed file to filter by regions

Molecular oncology Lab [ carlos.company.bioinf[at]gmail.com ]
```

### Demo

Basic command line

```
./sLCR_generation.py -i test/gen_test.bed  -n test_analysis \
    -t test/motifs_names.txt \ 
    -c data/regulatory_regions_CTCF_binding_sites.GRCh37.p13.bed \
    -r data/hg19_refseq_genes.txt \
    -g data/hg19.fa \
    -a data/hg19_weissman_primary_sorted.030418.bed \
    -m data/20130208_pwms.meme \
    -p 3
```

> Approx. CPU time 5m00s

## INSTALL

### Manual  

> It is also possible install individually the packages using conda. 

```
# Generate env
conda create --name slcr_env python=2.7
conda activate slcr_env

# Install packages
conda install -c bioconda pysam=0.15.2
conda install -c conda-forge pandas=0.24.2
conda install -c conda-forge numpy=1.16.3
conda install -c bioconda pybedtools=0.8.0
conda install -c conda-forge numba=0.43.1
conda install -c conda-forge biopython=1.73
conda install -c r r-rcolorbrewer=1.1_2
conda install -c conda-forge r-pheatmap=1.0.12
conda install -c r r-tidyverse=1.2.1
conda install -c bioconda bioconductor-genomicranges=1.38
conda install -c bioconda meme=5.0.2
```

### Conda environment

For easy installion please use [conda](https://docs.conda.io/projects/conda/en/latest/index.html) environment

```
conda create --name slcr_env --file env/sLCR_environment.txt
conda activate slcr_env
```
> Approx. CPU time 2m00s

## DEPENDENCIES 

* The LSD framework was built in python 2.7 and R 3.5. 
* Tested in CentosOS 6

* It depends on: 

``` python
import os, pysam,math
import pandas
import numpy as np
import multiprocessing as mp
import pybedtools
import Biopython
import numba
```

``` r
library(tidyverse)
library(RColorBrewer)
library(pheatmap)
library(GenomicRanges)
library(GenomeInfoDbData)
library(GenomeInfoDb)
```

## CONTACT 

Carlos Company < carlos.company.bioinf@gmail.com >  
Iros Barozzi < iros.barozzi@gmail.com > 
